import Vue from 'vue'
import VueRouter from 'vue-router'

import Login from './../components/Login/Login.vue'
import Home from './../components/Home/Home.vue'
import Branches from './../components/Branches/Branches.vue'
import CheckList from './../components/CheckList/CheckList.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '*',
    component: Home
  },
  {
    path: '/login',
    component: Login,
    meta: {
      forVisitors: true
    }
  },
  {
    path: '/sucursales',
    component: Branches,
    meta: {
      forAuth: true
    }
  },
  {
    path: '/checklist',
    component: CheckList,
    meta: {
      forAuth: true
    }
  }
]

export default new VueRouter({
  mode: 'history', // coment this line for local test
  routes
})
