/* let branches = [
        {
            id: "1",
            client: "Brunello Cucinelli",
            branch: "Masaryk",
            image: "bc.png",
            latitude: "19.432006",
            longitude: "-99.200354",
            color_primary: "#646569",
            color_secondary: "#6b6c70",
            full_name: "Brunello Cucinelli Masaryk",
            recent : 0,
            current: false,
            code: "bc"
        },
        {
            id: "2",
            client: "Brunello Cucinelli",
            branch: "ARTZ Pedregal",
            image: "bc.png",
            latitude: "19.313618",
            longitude: "-99.219106",
            color_primary: "#646569",
            color_secondary: "#6b6c70",
            full_name: "Brunello Cucinelli ARTZ Pedregal",
            recent : 0,
            current: false,
            code: "bc"
        },
        {
            id: "3",
            client: "Tommy Hilfiger",
            branch: "Antea",
            image: "th.png",
            latitude: "20.6753",
            longitude: "-100.436576",
            color_primary: "#001b4d",
            color_secondary: "#d50023",
            full_name: "Tommy Hilfiger Antea",
            recent : 0,
            current: false,
            code: "th"
        },
        {
            id: "4",
            client: "Tommy Hilfiger",
            branch: "Angelópolis Luxury Hall",
            image: "th.png",
            latitude: "19.032266",
            longitude: "-98.233784",
            color_primary: "#001b4d",
            color_secondary: "#d50023",
            full_name: "Tommy Hilfiger Angelópolis Luxury Hall",
            recent : 0,
            current: false,
            code: "th"
        },
        {
            id: "5",
            client: "Tommy Hilfiger",
            branch: "Galerías Monterrey",
            image: "th.png",
            latitude: "25.681794",
            longitude: "-100.354232",
            color_primary: "#001b4d",
            color_secondary: "#d50023",
            full_name: "Tommy Hilfiger Galerías Monterrey",
            recent : 0,
            current: false,
            code: "th"
        },
        {
            id: "6",
            client: "Tommy Hilfiger",
            branch: "Forum Culiacán",
            image: "th.png",
            latitude: "24.814688",
            longitude: "-107.399526",
            color_primary: "#001b4d",
            color_secondary: "#d50023",
            full_name: "Tommy Hilfiger Forum Culiacán",
            recent : 0,
            current: false,
            code: "th"
        },
        {
            id: "7",
            client: "Tommy Hilfiger",
            branch: "Andamar",
            image: "th.png",
            latitude: "19.139372",
            longitude: "-96.102087",
            color_primary: "#001b4d",
            color_secondary: "#d50023",
            full_name: "Tommy Hilfiger Andamar",
            recent : 0,
            current: false,
            code: "th"
        },
        {
            id: "8",
            client: "Tommy Hilfiger",
            branch: "La Isla Cancún",
            image: "th.png",
            latitude: "21.109989",
            longitude: "-86.763456",
            color_primary: "#001b4d",
            color_secondary: "#d50023",
            full_name: "Tommy Hilfiger La Isla Cancún",
            recent : 0,
            current: false,
            code: "th"
        },
        {
            id: "9",
            client: "Hollister",
            branch: "Andares",
            image: "hl.png",
             latitude: "20.710884",
            longitude: "-103.412278",
            color_primary: "#2c2f43",
            color_secondary: "#4b5065",
            full_name: "Hollister Andares",
            recent : 0,
            current: false,
            code: "hl"
        },
        {
            id: "10",
            client: "Hollister",
            branch: "Parque Delta",
            image: "hl.png",
            latitude: "19.402366",
            longitude: "-99.15453",
            color_primary: "#2c2f43",
            color_secondary: "#4b5065",
            full_name: "Hollister Parque Delta",
            recent : 0,
            current: false,
            code: "hl"
        },
        {
            id: "11",
            client: "Hollister",
            branch: "Playa Corazón",
            image: "hl.png",
            latitude: "20.627844",
            longitude: "-87.071596",
            color_primary: "#2c2f43",
            color_secondary: "#4b5065",
            full_name: "Hollister Playa Corazón",
            recent : 0,
            current: false,
            code: "hl"
        },
        {
            id: "12",
            client: "Victoria Secret's",
            branch: "Andares",
            image: "vs.png",
            latitude: "20.709825",
            longitude: "-103.411907",
            color_primary: "#dc4d84",
            color_secondary: "#e4a7ac",
            full_name: "Victoria Secret's Andares",
            recent : 0,
            current: false,
            code: "vs"
        },
        {
            id: "13",
            client: "Victoria Secret's",
            branch: "Antara Fashion Hall",
            image: "vs.png",
            latitude: "19.439464",
            longitude: "-99.20242",
            color_primary: "#dc4d84",
            color_secondary: "#e4a7ac",
            full_name: "Victoria Secret's Antara Fashion Hall",
            recent : 0,
            current: false,
            code: "vs"
        },
        {
            id: "14",
            client: "Victoria Secret's",
            branch: "Perisur",
            image: "vs.png",
            latitude: "19.304425",
            longitude: "-99.189211",
            color_primary: "#dc4d84",
            color_secondary: "#e4a7ac",
            full_name: "Victoria Secret's Perisur",
            recent : 0,
            current: false,
            code: "vs"
        },
        {
            id: "15",
            client: "Victoria Secret's",
            branch: "Santa Fé",
            image: "vs.png",
            latitude: "19.362143",
            longitude: "-99.272224",
            color_primary: "#dc4d84",
            color_secondary: "#e4a7ac",
            full_name: "Victoria Secret's Santa Fé",
            recent : 0,
            current: false,
            code: "vs"
        },
        {
            id: "16",
            client: "Victoria Secret's",
            branch: "Angelópolis Luxury Hall",
            image: "vs.png",
            latitude: "19.032008",
            longitude: "-98.232321",
            color_primary: "#dc4d84",
            color_secondary: "#e4a7ac",
            full_name: "Victoria Secret's Angelópolis Luxury Hall",
            recent : 0,
            current: false,
            code: "vs"
        },
        {
            id: "17",
            client: "Victoria Secret's",
            branch: "Antea Lifestyle Center",
            image: "vs.png",
            latitude: "20.674616",
            longitude: "-100.43639",
            color_primary: "#dc4d84",
            color_secondary: "#e4a7ac",
            full_name: "Victoria Secret's Antea Lifestyle Center",
            recent : 0,
            current: false,
            code: "vs"
        },
        {
            id: "18",
            client: "Axo",
            branch: "Corporativo",
            image: "axo.png",
            latitude: "19.455011",
            longitude: "-99.219211",
            color_primary: "#dc4d84",
            color_secondary: "#e4a7ac",
            full_name: "Axo Corporativo",
            recent : 0,
            current: false,
            code: "axo"
        },
        {
            id: "19",
            client: "Abercrombie & Fitch",
            branch: "Antara",
            image: "ab.png",
            latitude: "19.43903",
            longitude: "-99.201156",
            color_primary: "#3f6074",
            color_secondary: "#06436a",
            full_name: "Abercrombie & Fitch Antara",
            recent : 0,
            current: false,
            code: "ab"
        },
        {
            id: "20",
            client: "Bath & Body Works",
            branch: "Toreo",
            image: "bw.png",
            latitude: "19.45377",
            longitude: "-99.218886",
            color_primary: "#205383",
            color_secondary: "#a6bacd",
            full_name: "Bath & Body Works Toreo",
            recent : 0,
            current: false,
            code: "bw"
        },
        {
            id: "21",
            client: "Bath & Body Works",
            branch: "Antara",
            image: "bw.png",
            latitude: "19.439343",
            longitude: "-99.202273",
            color_primary: "#205383",
            color_secondary: "#a6bacd",
            full_name: "Bath & Body Works Antara",
            recent : 0,
            current: false,
            code: "bw"
        },
        {
            id: "22",
            client: "Brooks Brothers",
            branch: "Toreo",
            image: "bb.png",
            latitude: "19.453791",
            longitude: "-99.218659",
            color_primary: "#202a44",
            color_secondary: "#333b53",
            full_name: "Brooks Brothers Toreo",
            recent : 0,
            current: false,
            code: "bb"
        },
        {
            id: "23",
            client: "Brooks Brothers",
            branch: "Antara",
            image: "bb.png",
            latitude: "19.438868",
            longitude: "-99.201062",
            color_primary: "#202a44",
            color_secondary: "#333b53",
            full_name: "Brooks Brothers Antara",
            recent : 0,
            current: false,
            code: "bb"
        },
        {
            id: "24",
            client: "Brooks Brothers",
            branch: "Andares",
            image: "bb.png",
            latitude: "19.438868",
            longitude: "-99.201062",
            color_primary: "#202a44",
            color_secondary: "#333b53",
            full_name: "Brooks Brothers Andares",
            recent : 0,
            current: false,
            code: "bb"
        },
        {
            id: "25",
            client: "Calvin Klein",
            branch: "Toreo",
            image: "ck.png",
            latitude: "19.454565",
            longitude: "-99.21908",
            color_primary: "#3c3c3c",
            color_secondary: "#393637",
            full_name: "Calvin Klein Toreo",
            recent : 0,
            current: false,
            code: "ck"
        },
        {
            id: "26",
            client: "Calvin Klein",
            branch: "Andares",
            image: "ck.png",
            latitude: "20.709545",
            longitude: "-103.411967",
            color_primary: "#3c3c3c",
            color_secondary: "#393637",
            full_name: "Calvin Klein Andares",
            recent : 0,
            current: false,
            code: "ck"
        },
        {
            id: "27",
            client: "Coach",
            branch: "Antara",
            image: "co.png",
            latitude: "19.439182",
            longitude: "-99.202342",
            color_primary: "#000000",
            color_secondary: "#272727",
            full_name: "Coach Antara",
            recent : 0,
            current: false,
            code: "co"
        },
        {
            id: "28",
            client: "Coach",
            branch: "Andares",
            image: "co.png",
            latitude: "20.71053",
            longitude: "-103.413137",
            color_primary: "#000000",
            color_secondary: "#272727",
            full_name: "Coach Andares",
            recent : 0,
            current: false,
            code: "co"
        },
        {
            id: "29",
            client: "Express",
            branch: "Toreo",
            image: "ex.png",
            latitude: "19.453768",
            longitude: "-99.218504",
            color_primary: "#000000",
            color_secondary: "#3f3f3f",
            full_name: "Express Toreo",
            recent : 0,
            current: false,
            code: "ex"
        },
        {
            id: "30",
            client: "Express",
            branch: "Andares",
            image: "ex.png",
            latitude: "20.710258",
            longitude: "-103.411789",
            color_primary: "#000000",
            color_secondary: "#3f3f3f",
            full_name: "Express Andares",
            recent : 0,
            current: false,
            code: "ex"
        },
        {
            id: "31",
            client: "Guess",
            branch: "Toreo",
            image: "gs.png",
            latitude: "19.454196",
            longitude: "-99.218832",
            color_primary: "#e80031",
            color_secondary: "#000000",
            full_name: "Guess Toreo",
            recent : 0,
            current: false,
            code: "gs"
        },
        {
            id: "32",
            client: "Guess",
            branch: "Andares",
            image: "gs.png",
            latitude: "20.709761",
            longitude: "-103.411948",
            color_primary: "#e80031",
            color_secondary: "#000000",
            full_name: "Guess Andares",
            recent : 0,
            current: false,
            code: "gs"
        },
        {
            id: "33",
            client: "Kate Spade",
            branch: "Antara",
            image: "ks.png",
            latitude: "19.439529",
            longitude: "-99.202228",
            color_primary: "#ff6f83",
            color_secondary: "#65a522",
            full_name: "Kate Spade Antara",
            recent : 0,
            current: false,
            code: "ks"
        },
        {
            id: "34",
            client: "Loft",
            branch: "Toreo",
            image: "lf.png",
            latitude: "19.45421",
            longitude: "-99.21846",
            color_primary: "#7b7b7d",
            color_secondary: "#f57b8a",
            full_name: "Loft Toreo",
            recent : 0,
            current: false,
            code: "lf"
        },
        {
            id: "35",
            client: "Rapsodia",
            branch: "Antara",
            image: "rp.png",
            latitude: "19.45421",
            longitude: "-99.21846",
            color_primary: "#ae9770",
            color_secondary: "#3e431f",
            full_name: "Rapsodia Antara",
            recent : 0,
            current: false,
            code: "rp"
        },
        {
            id: "36",
            client: "Rapsodia",
            branch: "Andares",
            image: "rp.png",
            latitude: "20.710182",
            longitude: "-103.412366",
            color_primary: "#ae9770",
            color_secondary: "#3e431f",
            full_name: "Rapsodia Andares",
            recent : 0,
            current: false,
            code: "rp"
        },
        {
            id: "37",
            client: "Tommy Hilfiger",
            branch: "Toreo",
            image: "th.png",
            latitude: "19.454031",
            longitude: "-99.218485",
            color_primary: "#001b4d",
            color_secondary: "#d50023",
            full_name: "Tommy Hilfiger Toreo",
            recent : 0,
            current: false,
            code: "th"
        },
        {
            id: "38",
            client: "Tommy Hilfiger",
            branch: "Andares",
            image: "th.png",
            latitude: "20.709875",
            longitude: "-103.412057",
            color_primary: "#001b4d",
            color_secondary: "#d50023",
            full_name: "Tommy Hilfiger Andares",
            recent : 0,
            current: false,
            code: "th"
        },
        {
            id: "39",
            client: "Victoria Secret's",
            branch: "Toreo",
            image: "vs.png",
            latitude: "19.454416",
            longitude: "-99.218778",
            color_primary: "#dc4d84",
            color_secondary: "#e4a7ac",
            full_name: "Victoria Secret's Toreo",
            recent : 0,
            current: false,
            code: "vs"
        },
    ] */

/* let questions = [
        {
            id: 1,
            category: "ESCAPARATES",
            questions: [
                {
                    id : 1,
                    question : "¿El escaparate se encuentra limpio y con pintura en buen estado?",
                    answer : '',
                    imgUrl :'',
                    requiredPhoto : 1,
                    comment: ""
                },
                {
                    id: 2,
                    question: "¿Las luces están enfocadas correctamente?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 1,
                    comment: ""
                },
                {
                    id: 3,
                    question: "¿El producto expuesto es el correcto teniendo en cuenta estacionalidad y tipo de cliente del CC?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 1,
                    comment: ""
                },
                {
                    id: 4,
                    question: "¿Los precios están correctamente expuestos y son visibles?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 1,
                    comment: ""
                },
                {
                    id: 5,
                    question: "En caso de estar presente carteleria de promoción, ¿el mensaje es correcto y claro?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 1,
                    comment: ""
                },
            ]
        },
        {
            id: 2,
            category: "ENTRADA",
            questions: [
                {
                    id: 1,
                    question: "¿El producto expuesto en la peana/bienvenida/ entrada es el adecuado por estacionalidad y tipo de cliente?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 1,
                    comment: ""
                },
                {
                    id: 2,
                    question: "¿Es fácil localizar el producto expuesto en la peana?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 1,
                    comment: ""
                },
                {
                    id: 3,
                    question: "¿La exposición facilita la venta cruzada?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 1,
                    comment: ""
                }
            ]
        },
        {
            id: 3,
            category: "SEGURIDAD",
            questions: [
                {
                    id: 1,
                    question: "¿El elemento de seguridad se encuentra en su lugar?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 1,
                    comment: ""
                },
                {
                    id: 2,
                    question: "¿Mantiene la actitud adecuada?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 0,
                    comment: ""
                },
                {
                    id: 3,
                    question: "¿Saluda y da la bienvenida a los clientes al entrar?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 0,
                    comment: ""
                }
            ]
        },
        {
            id: 4,
            category: "LIMPIEZA",
            questions: [
                {
                    id: 1,
                    question: "¿La tienda se encuentra limpia y libre de cajas?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 1,
                    comment: ""
                },
                {
                    id: 2,
                    question: "¿El aroma es el que corresponde?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 0,
                    comment: ""
                }
            ]
        },
        {
            id: 5,
            category: "MÚSICA",
            questions: [
                {
                    id: 1,
                    question: "¿La música es la que corresponde según el playlist de la marca?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 0,
                    comment: ""
                },
                {
                    id: 2,
                    question: "¿El volumen es el adecuado?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 0,
                    comment: ""
                }
            ]
        },
        {
            id: 6,
            category: "ILUMINACION",
            questions: [
                {
                    id: 1,
                    question: "¿La tienda se encuentra completamente iluminada?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 1,
                    comment: ""
                },
                {
                    id: 2,
                    question: "¿Las luces están enfocadas correctamente?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 1,
                    comment: ""
                }
            ]
        },
        {
            id: 7,
            category: "PRODUCTO",
            questions: [
                {
                    id: 1,
                    question: "¿La tienda esta repuesta al 100%?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 1,
                    comment: ""
                },
                {
                    id: 2,
                    question: "¿El producto esta correctamente preciado, alarmado y tallado?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 1,
                    comment: ""
                },
                {
                    id: 3,
                    question: "¿El producto nuevo se encuentra expuesto en tienda?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 1,
                    comment: ""
                },
                {
                    id: 4,
                    question: "¿La mercancía se encuentra correctamente colgada/doblada/perfilada y planchada?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 1,
                    comment: ""
                }
            ]
        },
        {
            id: 8,
            category: "PROMOCIONES",
            questions: [
                {
                    id: 1,
                    question: "¿La carteleria de promoción expuesta esta en buen estado y es clara en su mensaje?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 1,
                    comment: ""
                },
                {
                    id: 2,
                    question: "¿Todos los artículos están preciados y la etiqueta cumple normativa de Profeco?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 1,
                    comment: ""
                }
            ]
        },
        {
            id: 9,
            category: "PUNTO DE COBRO",
            questions: [
                {
                    id: 1,
                    question: "¿La caja se encuentra recogida y limpia de producto?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 1,
                    comment: ""
                },
                {
                    id: 2,
                    question: "¿Esta expuesta la informacion requerida para el PDV (licencia de funcionamiento, cobro en dolares…)?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 1,
                    comment: ""
                },
                {
                    id: 3,
                    question: "¿El cableado de POS y datafonos se encuentra recogido y no presenta riesgo para el PDV?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 1,
                    comment: ""
                },
                {
                    id: 4,
                    question: "¿La tienda cuenta con suficiente stock de shopping bags?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 1,
                    comment: ""
                },
            ]
        },
        {
            id: 10,
            category: "PROBADORES",
            questions: [
                {
                    id: 1,
                    question: "¿Los probadores se encuentran limpios de ropa y perchas?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 1,
                    comment: ""
                },
                {
                    id: 2,
                    question: "¿Cuentan con la iluminacion adecuada y los espejos están en buen estado?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 1,
                    comment: ""
                },
                {
                    id: 3,
                    question: "¿El mantenimiento (pintura, mobiliario) esta en el estado correcto?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 1,
                    comment: ""
                },
                {
                    id: 4,
                    question: "¿Los probadores se encuentran limpios de cajas y mobiliario adicional?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 1,
                    comment: ""
                },
            ]
        },
        {
            id: 11,
            category: "BODEGA",
            questions: [
                {
                    id: 1,
                    question: "¿Las zonas de transito se encuentran libres de cajas?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 1,
                    comment: ""
                },
                {
                    id: 2,
                    question: "¿La salida de emergencia esta despejada y cumple requisitos de seguridad en caso de evacuación?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 1,
                    comment: ""
                },
                {
                    id: 3,
                    question: "¿Los anaqueles están ordenados por categorias y tipo de producto?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 1,
                    comment: ""
                },
                {
                    id: 4,
                    question: "¿El producto se encuentra identificado y entallado?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 1,
                    comment: ""
                },
                {
                    id: 5,
                    question: "¿La iluminacion es la correcta?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 1,
                    comment: ""
                },
                {
                    id: 6,
                    question: "¿El producto de visual se encuentra colocado y ordenado en el lugar que corresponde?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 1,
                    comment: ""
                },
                {
                    id: 7,
                    question: "¿El rack, el cableado y los aparatos electricos cumplen las medidad de seguridad?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 1,
                    comment: ""
                },
                {
                    id: 8,
                    question: "¿Las áreas comunes están limpias y ordenadas?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 1,
                    comment: ""
                },
                {
                    id: 9,
                    question: "¿Los lockers se encuentran cerrados y con candados, con todo en su interior?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 1,
                    comment: ""
                },
            ]
        },
        {
            id: 12,
            category: "EQUIPO",
            questions: [
                {
                    id: 1,
                    question: "¿El equipo porta el uniforme o, en su lugar, el outfit autorizado que representa la imagen de la marca?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 1,
                    comment: ""
                },
                {
                    id: 2,
                    question: "¿La imagen personal (limpieza, maquillaje, cabello...) es el adecuado?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 1,
                    comment: ""
                },
                {
                    id: 3,
                    question: "¿El personal se encuentra distribuido por zonas?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 0,
                    comment: ""
                },
                {
                    id: 4,
                    question: "¿Cada miembro del equipo conoce sus objetivos de venta globales e individuales?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 0,
                    comment: ""
                },
                {
                    id: 5,
                    question: "¿El equipo tiene vacantes? ¿Qué posiciones? ¿Desde cuando?",
                    answer: '',
                    imgUrl:'',
                    requiredPhoto: 0,
                    comment: ""
                },
            ]
        }
    ] */
