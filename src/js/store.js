import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
  isAnswering: localStorage.getItem('answering')
    ? parseInt(localStorage.getItem('answering'))
    : false,
  selectedBranch: localStorage.getItem('selectedBranch')
    ? JSON.parse(localStorage.getItem('selectedBranch'))
    : null,
  selectedCategory: null,
  userInfo: localStorage.getItem('userInfo')
    ? localStorage.getItem('userInfo')
    : null
}

const mutations = {
  SET_ANSWERING (state, value) {
    state.isAnswering = value
  },
  SET_SELECTED_BRANCH (state, value) {
    state.selectedBranch = value
  },
  SET_SELECTED_CATEGORY (state, value) {
    state.selectedCategory = value
  },
  SET_USER_INFO (state, value) {
    state.userInfo = value
  }
}

// call for get value
const getters = {
  getAnswering (state) {
    return state.isAnswering
  },
  getSelectedBranch (state) {
    return state.selectedBranch
  },
  getSelectedCategory (state) {
    return state.selectedCategory
  },
  getUserInfo (state) {
    return state.userInfo
  }
}

// call for change state
const actions = {
  setAnswering (state, value) {
    state.commit('SET_ANSWERING', value)
  },
  setSelectedBranch (state, value) {
    state.commit('SET_SELECTED_BRANCH', value)
  },
  setSelectedCategory (state, value) {
    state.commit('SET_SELECTED_CATEGORY', value)
  },
  setUserInfo (state, value) {
    state.commit('SET_USER_INFO', value)
  }
}

export default new Vuex.Store({
  state,
  mutations,
  getters,
  actions
})
