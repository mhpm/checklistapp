import Vue from 'vue'
import router from './router/main'
import Auth from './auth/Auth.js'
import store from './store'
import EventBus from './plugins/EventBus'

import HeaderContent from './components/Includes/HeaderContent.vue'
import SidebarContent from './components/Includes/SidebarContent.vue'

window.axios = require('axios')
window.is = require('is_js')
window.$ = window.jQuery = require('jquery')
window.APP_NAME = 'Gran Via'
window.URL_SERVER = 'https://gvchecklistapi.trainupquiz.com'
window.CLIENT_SECRET = 'JmW1CNwYC5sO2q0j4adR46wFxJJbdIw8z40ms1LR'
window.LOGO = 'granvia.png'

if (window.location.hostname == 'gvchecklist.trainupquiz.com') {
  // for production
  window.APP_NAME = 'Gran Via'
  window.URL_SERVER = 'https://gvchecklistapi.trainupquiz.com'
  window.LOGO = 'granvia.png'
} else if (window.location.hostname == 'demochecklist.trainupquiz.com') {
  // for demo
  window.URL_SERVER = 'https://demochecklistapi.trainupquiz.com/'
} else {
  // for localhost
  window.URL_SERVER = 'https://10659991.ngrok.io'
}

// Vue.component('image-uploader', ImageUploader)

Vue.use(Auth)
Vue.use(EventBus)
// Vue.mixin({
//   data () {
//     return {}
//   },
//   methods: {
//     globalReadOnlyProperty () {
//       return 'hello from globaly'
//     }
//   }
// })

if (localStorage.getItem('token') != null) {
  axios.defaults.headers.common['Authorization'] =
    'Bearer ' + Vue.auth.getToken()
}
axios.defaults.headers.post['Content-Type'] = 'application/json'

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.forVisitors)) {
    if (Vue.auth.isAuthenticated()) {
      next({
        path: '/sucursales'
      })
    } else next()
  } else if (to.matched.some(record => record.meta.forAuth)) {
    if (!Vue.auth.isAuthenticated()) {
      next({
        path: '/login'
      })
    } else if (to.path == '/sucursales' && window.isAnswering()) {
      next({
        path: '/checklist'
      })
    } else next()
  } else next()
})

window.theme = function () {
  let theme = localStorage.getItem('theme') || ''
  return theme
}

window.isAnswering = function () {
  let answering = localStorage.getItem('answering') || ''
  return answering == '1'
}

const content = new Vue({
  el: '#app',
  components: {
    HeaderContent,
    SidebarContent
  },
  router,
  store,
  data: {
    successMsg: ''
  },
  methods: {
    openSidebar () {
      let sidebar = document.querySelector('.sidebar')
      let navToggle = document.querySelector('.nav-toggle')

      sidebar.classList.toggle('open-sidebar')
      navToggle.classList.toggle('active')
    }
  },
  computed: {
    url () {
      return this.$route.path
    },

    hasToShowHeader () {
      let show_header = false

      switch (this.url.split('/')[1]) {
        case 'sucursales':
          show_header = true
          break
        case 'checklist':
          show_header = true
          break
      }

      return show_header
    }
  },
  watch: {
    successMsg () {
      if (content.successMsg != '') {
        setTimeout(function () {
          content.successMsg = ''
        }, 4500)
      }
    }
  }
})

window.content = content

$(document).mouseup(function (e) {
  var menu_container = $('.menu')

  if (
    !menu_container.is(e.target) &&
    menu_container.has(e.target).length === 0
  ) {
    if ($('.nav-toggle').hasClass('active')) content.openSidebar()
  }
})

window.processStrJson = function (str, htmlPre, htmlPos) {
  if (str == '') return ''

  var htmlPre = htmlPre || '* '
  var htmlPos = htmlPos || '<br/>'

  if (typeof str === 'string') {
    return htmlPre + str + htmlPos
  } else if (Array.isArray(str) || typeof str === 'object') {
    var output = ''
    for (var objt in str) {
      for (var i = 0; i <= str[objt].length - 1; i++) {
        var value = str[objt][i]

        if (value != '') {
          output += htmlPre + value + htmlPos
        }
      }
    }

    return output
  }
  return ''
}

window.getDistanceFromLatLonInKm = function (lat1, lon1, lat2, lon2) {
  var R = 6371 // Radius of the earth in km
  var dLat = deg2rad(lat2 - lat1) // deg2rad below
  var dLon = deg2rad(lon2 - lon1)
  var a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(lat1)) *
      Math.cos(deg2rad(lat2)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2)
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
  var d = R * c // Distance in km
  return d
}

window.deg2rad = function (deg) {
  return deg * (Math.PI / 180)
}

window.delayKeyup = function () {
  let timer = 0
  return function (callback, ms) {
    clearTimeout(timer)
    timer = setTimeout(callback, ms)
  }
}
