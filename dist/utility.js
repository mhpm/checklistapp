var dbPromise = idb.open('grupoaxo-store', 6, function (db) {
  if (!db.objectStoreNames.contains('user')) {
    db.createObjectStore('user', { keyPath: 'id' })
  }
  if (!db.objectStoreNames.contains('branches')) {
    db.createObjectStore('branches', { keyPath: 'id' })
  }
  if (!db.objectStoreNames.contains('questions')) {
    db.createObjectStore('questions', { keyPath: 'id' })
  }
})

function writeData (st, data) {
  return dbPromise.then(function (db) {
    var tx = db.transaction(st, 'readwrite')
    var store = tx.objectStore(st)
    store.put(data)
    return tx.complete
  })
}

function readAllData (st) {
  return dbPromise.then(function (db) {
    var tx = db.transaction(st, 'readonly')
    var store = tx.objectStore(st)
    return store.getAll()
  })
}

function readItemData (st, id) {
  return dbPromise.then(function (db) {
    var tx = db.transaction(st, 'readonly')
    var store = tx.objectStore(st)

    return store.get(id)
  })
}

function clearAllData (st) {
  return dbPromise.then(function (db) {
    var tx = db.transaction(st, 'readwrite')
    var store = tx.objectStore(st)
    store.clear()
    return tx.complete
  })
}

function deleteItemFromData (st, id) {
  dbPromise
    .then(function (db) {
      var tx = db.transaction(st, 'readwrite')
      var store = tx.objectStore(st)
      store.delete(id)
      return tx.complete
    })
    .then(function () {
      console.log('Item deleted!')
    })
}
